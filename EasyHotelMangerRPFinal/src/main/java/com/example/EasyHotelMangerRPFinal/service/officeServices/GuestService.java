package com.example.EasyHotelMangerRPFinal.service.officeServices;
import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.GuestDTO;
import com.example.EasyHotelMangerRPFinal.exception.ResourceNotFoundException;
import com.example.EasyHotelMangerRPFinal.model.person.Guest;
import com.example.EasyHotelMangerRPFinal.repository.GuestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;

@Service

public class GuestService {
    @Autowired
    GuestRepository guestRepository;

    //get all guests
    public List<Guest> getAllGuests() {
        return guestRepository.findAll();
    }

    //get guest by id
    public ResponseEntity<Guest> getGuestById(@PathVariable long id) {
        Guest guest = guestRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Guest with id " + id + " doesn´t exist!"));

        return ResponseEntity.ok(guest);
    }

    //post new guest
    public void postGuest(Guest guest) {
        guestRepository.save(guest);
    }

    //delete guest by id
    public void deleteGuest(@PathVariable long id) {
        Guest guest = guestRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Guest with id " + id + "doesn´t exist!"));
        guestRepository.delete(guest);
    }

    //put guest DTO
    public void put(long id, GuestDTO guestDTO) {
        Guest guest = guestRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Guest with id " + id + "doesn´t exist!"));

        guest.setSalutation(guestDTO.getSalutation());
        guest.setFirstName(guestDTO.getFirstName());
        guest.setLastName(guestDTO.getLastName());
        guest.setEMailAddress(guestDTO.getEMailAddress());
        guest.setAge(guestDTO.getAge());
        guest.setPayment(guestDTO.getPayment());
        guest.setPhoneNumber(guestDTO.getPhoneNumber());
        guest.setBooking(guestDTO.getBooking());
        guestRepository.save(guest);
    }

    // GuestDTO to Guest - converter
    public Guest convertGuestDTOtoGuest(GuestDTO guestDTO) {
        Guest guest = new Guest();
        guest.setSalutation(guestDTO.getSalutation());
        guest.setFirstName(guestDTO.getFirstName());
        guest.setLastName(guestDTO.getLastName());
        guest.setEMailAddress(guestDTO.getEMailAddress());
        guest.setAge(guestDTO.getAge());
        guest.setPayment(guestDTO.getPayment());
        guest.setPhoneNumber(guestDTO.getPhoneNumber());

        return guest;
    }
}
