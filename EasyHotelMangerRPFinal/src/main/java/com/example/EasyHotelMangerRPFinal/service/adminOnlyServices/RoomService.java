package com.example.EasyHotelMangerRPFinal.service.adminOnlyServices;

import com.example.EasyHotelMangerRPFinal.dto.hotelDTOs.ChangeRoomPricePerNightDTO;
import com.example.EasyHotelMangerRPFinal.dto.hotelDTOs.RoomDTO;
import com.example.EasyHotelMangerRPFinal.exception.ResourceNotFoundException;
import com.example.EasyHotelMangerRPFinal.model.hotel.Hotel;
import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import com.example.EasyHotelMangerRPFinal.model.hotel.RoomCategory;
import com.example.EasyHotelMangerRPFinal.repository.HotelRepository;
import com.example.EasyHotelMangerRPFinal.repository.RoomRepository;
import jdk.jfr.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service

public class RoomService {

    @Autowired
    RoomRepository roomRepository;
    @Autowired
    HotelRepository hotelRepository;

    //get all rooms
    public List<Room> getAllRooms(){return roomRepository.findAll();}

    //get room by id
    public ResponseEntity<Room> getRoomById(@PathVariable long id) {
        Room room = roomRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Hotel with id " + id + " doesn´t exist!"));

        return ResponseEntity.ok(room);
    }

    //delete room by id
    public void deleteRoom(@PathVariable long id) {
        Room room = roomRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(("Room with id " + id + " + doesn´t exist!")));

        room.getHotel().getRoomList().remove(room);
        room.getHotel().setNumberOfRooms(room.getHotel().getRoomList().size());
        hotelRepository.save(room.getHotel());
        roomRepository.delete(room);
    }

    //post new room
    public void postRoom(long hotelId, int categorie) {
        Hotel hotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new ResourceNotFoundException("Hotel with id " + hotelId + " doesn´t exist!"));
        Room room = new Room();
        if(categorie > 0 && categorie <= RoomCategory.values().length) {
            switch (categorie) {
                case 1:
                    room.setRoomCategory(RoomCategory.CATEGORY1);
                    break;
                case 2:
                    room.setRoomCategory(RoomCategory.CATEGORY2);
                    break;
                case 3:
                    room.setRoomCategory(RoomCategory.CATEGORY3);
                    break;
                case 4:
                    room.setRoomCategory(RoomCategory.CATEGORY4);
                    break;
                default:
                    System.out.println("Cateogrie " + categorie + " not found, please choose a category between 1 and "
                            + RoomCategory.values().length + "!");
            }
            room.setHotel(hotelRepository.findById(hotelId).get());
            hotelRepository.findById(hotelId).get().addRoomToList(room);
            room.setNumberOfBeds(room.getRoomCategory().getNumberOfBeds());
            room.setNumberOfGuests(room.getRoomCategory().getNumberOfGuests());
            room.setOffSeasonPricePerNightByRoomCateogry(room.getRoomCategory());
            room.setMainSeasonPricePerNightByRoomCategory(room.getRoomCategory());

            roomRepository.save(room);
        } else {
            System.out.println("Cateogrie " + categorie + " not found, please choose a category between 1 and "
                    + RoomCategory.values().length + "!");
        }
    }

    //put room
    public void putRoom(@PathVariable long id, RoomDTO roomDTO ){
        Room room = roomRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(("Room with id " + id + " + doesn´t exist!")));

         if(room.getRoomCategory() != roomDTO.getRoomCategory()) {
             room.setRoomCategory(roomDTO.getRoomCategory());
             room.setNumberOfBeds(roomDTO.getRoomCategory().getNumberOfBeds());
             room.setNumberOfGuests(roomDTO.getRoomCategory().getNumberOfGuests());
             room.setOffSeasonPricePerNightByRoomCateogry(roomDTO.getRoomCategory());
             room.setMainSeasonPricePerNightByRoomCategory(roomDTO.getRoomCategory());
         }
         roomRepository.save(room);
    }

    //change off season price per night. standard price is set by enum RoomCategory
    public void changeOffSeasonPricePerNight(ChangeRoomPricePerNightDTO changeRoomPricePerNightDTO){
        Room room = roomRepository.findById(changeRoomPricePerNightDTO.getRoomId())
                .orElseThrow(() -> new ResourceNotFoundException(("Room with id " + changeRoomPricePerNightDTO.getRoomId() + " + doesn´t exist!")));

        room.setOffSeasonPricePerNight(changeRoomPricePerNightDTO.getRoomPricePerNight());
        room.setMainSeasonPricePerNight(changeRoomPricePerNightDTO.getRoomPricePerNight() * 1.1);
        roomRepository.save(room);
    }
}
