package com.example.EasyHotelMangerRPFinal.service.officeServices;

import com.example.EasyHotelMangerRPFinal.controller.officeController.BookingController;
import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.StornoDTO;
import com.example.EasyHotelMangerRPFinal.exception.ResourceNotFoundException;
import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import com.example.EasyHotelMangerRPFinal.model.booking.Storno;
import com.example.EasyHotelMangerRPFinal.model.booking.StornoCalculator;
import com.example.EasyHotelMangerRPFinal.repository.BookingRepository;
import com.example.EasyHotelMangerRPFinal.repository.StornoRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.util.List;
@Service

public class StornoService {
    @Autowired
    StornoRepository stornoRepository;
    @Autowired
    BookingRepository bookingRepository;
    @Autowired
    BookingController bookingController;

    //get all Stornos
    public List<Storno> getAllStornos() {
        return stornoRepository.findAll();
    }
    //get Storno by Id
    public ResponseEntity<Storno> getStornoById(@PathVariable long id) {
        Storno storno = stornoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Storno with id " + id + " doesn´t exist!"));
        return ResponseEntity.ok(storno);
    }

    //delete storno by Id
    public void deleteStornoById(@PathVariable long id) {
        Storno storno = stornoRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Storno with id " + id + " doesn´t exist!"));
        stornoRepository.delete(storno);
    }

    //calculate storno by booking id
    public StornoDTO getStornoCalculationByBookingId(@PathVariable long id, StornoDTO stornoDTO) {

        LocalDate dateOfStorno = stornoDTO.getDateOfStorno();
        Booking booking = bookingRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Booking with id " + id + " not found"));

        stornoDTO.setDateOfReservation(bookingRepository.findById(id).get().getDateOfReservation());
        stornoDTO.setDateOfPlannedCheckin(bookingRepository.findById(id).get().getCheckIn());
        stornoDTO.setBookingSum(bookingRepository.findById(id).get().getPriceTotal());

        StornoCalculator stornoCalculator = new StornoCalculator();

        stornoDTO.setStornoSum(stornoCalculator.calculatingStorno(bookingRepository.findById(id).get(), dateOfStorno));

        return stornoDTO;
    }

    //post a new storno
    public void postStorno(@PathVariable long id, StornoDTO stornoDTO){

        Booking booking = bookingRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Booking with id " + id + " not found"));

        StornoDTO stornoDTO1 = getStornoCalculationByBookingId(id, stornoDTO);
        ModelMapper modelMapper = new ModelMapper();
        Storno storno = modelMapper.map(stornoDTO1, Storno.class);

        storno.setRoom(bookingRepository.findById(1L).get().getRoom());
        bookingController.deleteBooking(id);
        stornoRepository.save(storno);
    }
}
