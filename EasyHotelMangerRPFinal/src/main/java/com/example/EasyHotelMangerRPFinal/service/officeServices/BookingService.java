package com.example.EasyHotelMangerRPFinal.service.officeServices;

import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.AddGuestToBookingDTO;
import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.BookingDTO;
import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.CheckAvailableRoomsDTO;
import com.example.EasyHotelMangerRPFinal.exception.ResourceNotFoundException;
import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import com.example.EasyHotelMangerRPFinal.model.booking.BookingPriceCalculator;
import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import com.example.EasyHotelMangerRPFinal.model.person.Guest;
import com.example.EasyHotelMangerRPFinal.repository.BookingRepository;
import com.example.EasyHotelMangerRPFinal.repository.GuestRepository;
import com.example.EasyHotelMangerRPFinal.repository.RoomRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service

public class BookingService {
    @Autowired
    BookingRepository bookingRepository;
    @Autowired
    RoomRepository roomRepository;
    @Autowired
    GuestRepository guestRepository;

    //get all bookings
    public List<Booking> getAllBookings() {
        return bookingRepository.findAll();
    }

    //get booking by id
    public ResponseEntity<Booking> getBookingById(@PathVariable long id) {
        Booking booking = bookingRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Booking with id " + id + " not found"));

        return ResponseEntity.ok(booking);
    }

    //delete booking by id
    public void deleteBooking(@PathVariable long id) {
        Booking booking = bookingRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Booking with id " + id + " not found"));

        booking.getGuestList().stream().forEach(e -> e.setBooking(null));
        booking.getRoom().getBookingList().remove(booking);
        bookingRepository.deleteById(id);
    }

    // put booking DTO
    public void putBooking(long id, BookingDTO bookingDTO) {
        BookingPriceCalculator bookingPriceCalculator = new BookingPriceCalculator();

        Booking booking = bookingRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Booking with id " + id + " not found"));
        bookingDTO.setId(id);

        if (bookingDTO.getPlannedNumberOfGuests() != booking.getGuestListSize()) {
            if (bookingDTO.getPlannedNumberOfGuests() <= booking.getRoom().getNumberOfGuests()) {
                booking.setGuestListSize(bookingDTO.getPlannedNumberOfGuests());
            } else {
                System.out.println("Room cannot contain more guests");
            }
        } else if ((booking.getCheckIn() != bookingDTO.getCheckIn()) || (booking.getCheckOut() != bookingDTO.getCheckOut())) {
            if (isAvailableBetweenPut(bookingDTO, bookingRepository.findById(id).get())) {

                booking.setCheckIn(bookingDTO.getCheckIn());
                booking.setCheckOut(bookingDTO.getCheckOut());
                booking.setBookedDays(ChronoUnit.DAYS.between(booking.getCheckIn(), booking.getCheckOut()));
                roomRepository.findById(bookingDTO.getRoom().getId()).get().addBooking(booking);
                booking.setPriceTotal(bookingPriceCalculator.calculateBookingPrice(booking.getCheckIn(), booking.getCheckOut(), booking.getRoom()));
                booking.setPricePerNight(bookingPriceCalculator.calculatePricePerNight(booking.getCheckIn(), booking.getCheckOut(), booking.getRoom()));
                bookingRepository.save(booking);
            } else {
                System.out.println("Error while postBooking(): Room is already booked!");
            }
        }
    }

    //post new booking
    public void postBooking(BookingDTO bookingDTO) {
        BookingPriceCalculator bookingPriceCalculator = new BookingPriceCalculator();

        ModelMapper modelMapper = new ModelMapper();
        Booking booking = modelMapper.map(bookingDTO, Booking.class);

        LocalDate checkIn = bookingDTO.getCheckIn();
        LocalDate checkOut = bookingDTO.getCheckOut();

        Room room = roomRepository.findById(booking.getRoom().getId()).get();

        if (room.getNumberOfGuests() >= booking.getGuestListSize()) {
            if (isAvailableBetween(checkIn, checkOut, room)) {

                room.addBooking(booking);
                booking.setBookedDays(ChronoUnit.DAYS.between(checkIn, checkOut));
                booking.setPriceTotal(bookingPriceCalculator.calculateBookingPrice(checkIn, checkOut, room));
                booking.setPricePerNight(bookingPriceCalculator.calculatePricePerNight(checkIn, checkOut, room));
                bookingRepository.save(booking);

            } else {
                System.out.println("Error while postBooking(): Room is already booked!");
            }
        } else {
            System.out.println("Error while postBooking(): Room cannot contain more guests!");
        }
    }

    //returns boolean, whether room is available between planed checkin & checkout for postmapping
    public boolean isAvailableBetween(LocalDate checkIn, LocalDate checkOut, Room room) {

        return roomRepository.findById(room.getId())
                .get()
                .getBookingList()
                .stream()
                .noneMatch(b -> (checkIn.isBefore(b.getCheckOut()) && checkOut.isAfter(b.getCheckOut()))
                        || (checkIn.isBefore(b.getCheckIn()) && checkOut.isAfter(b.getCheckIn()))
                        || (checkIn.isAfter(b.getCheckIn()) && checkOut.isBefore(b.getCheckOut()))
                        || (checkIn.isBefore(b.getCheckIn()) && checkOut.isAfter(b.getCheckOut()))
                        || checkIn.isEqual(b.getCheckIn()) || checkOut.isEqual(b.getCheckOut()));
    }

    //returns boolean, whether room is available between planed checkin & checkout for putmapping
    public boolean isAvailableBetweenPut(BookingDTO bookingDTO, Booking booking) {

        return bookingRepository.findById(booking.getId())
                .get().getRoom()
                .getBookingList()
                .stream()
                .noneMatch(b -> b.getId() != bookingDTO.getId()
                        && ((bookingDTO.getCheckIn().isBefore(b.getCheckOut()) && bookingDTO.getCheckOut().isAfter(b.getCheckOut()))
                        || ((bookingDTO.getCheckIn().isBefore(b.getCheckIn()) && bookingDTO.getCheckOut().isAfter(b.getCheckIn())))
                        || ((bookingDTO.getCheckIn().isAfter(b.getCheckIn()) && bookingDTO.getCheckOut().isBefore(b.getCheckOut())))
                        || ((bookingDTO.getCheckIn().isBefore(b.getCheckIn()) && bookingDTO.getCheckOut().isAfter(b.getCheckOut())))
                        || (bookingDTO.getCheckIn().isEqual(b.getCheckIn()) || bookingDTO.getCheckOut().isEqual(b.getCheckOut()))));

    }

    //returns list of available rooms between planed checkin & checkout
    public List<Room> listOfAvailableRoomsBetween(CheckAvailableRoomsDTO checkAvailableRoomsDTO) {
        LocalDate checkIn = checkAvailableRoomsDTO.getCheckIn();
        LocalDate checkOut = checkAvailableRoomsDTO.getCheckOut();

        return roomRepository.findAll()
                .stream()
                .filter(r -> r.getBookingList()
                        .stream()
                        .allMatch(b -> isAvailableBetween(checkIn, checkOut, r)))
                .toList();
    }

    //add new guest to booking
    public void addGuestToBooking(AddGuestToBookingDTO addGuestToBookingDTO) {

        Guest guest = guestRepository.findById(addGuestToBookingDTO.getGuestId())
                .orElseThrow(() -> new ResourceNotFoundException
                        ("Guest with id " + addGuestToBookingDTO.getGuestId() + " doesn´t exist!"));

        Booking booking = bookingRepository.findById(addGuestToBookingDTO.getBookingId())
                .orElseThrow(() -> new ResourceNotFoundException
                        ("Booking with id " + addGuestToBookingDTO.getBookingId() + " not found"));

        if (booking.getRoom().getNumberOfGuests() > booking.getGuestList().size()) {
            if(!booking.getGuestList().contains(guest)) {
                booking.addGuest(guest);
                guest.setBooking(booking);
                booking.setGuestListSize(booking.getGuestList().size());
                bookingRepository.save(booking);
            } else {
                System.out.println("Error at BookingService - addGuestToBooking(): guest already registered!");
            }
        } else {
            System.out.println("Error at BookingService - addGuestToBooking(): reached guest limit!");
        }
    }

    //remove guest from booking
    public void removeGuestFromBooking(AddGuestToBookingDTO addGuestToBookingDTO) {
        Guest guest = guestRepository.findById(addGuestToBookingDTO.getGuestId())
                .orElseThrow(() -> new ResourceNotFoundException
                        ("Guest with id " + addGuestToBookingDTO.getGuestId() + " doesn´t exist!"));

        Booking booking = bookingRepository.findById(addGuestToBookingDTO.getBookingId())
                .orElseThrow(() -> new ResourceNotFoundException
                        ("Booking with id " + addGuestToBookingDTO.getBookingId() + " not found"));

        booking.getGuestList().remove(guest);
        booking.setGuestListSize(booking.getGuestList().size());
        booking.addGuest(guest);
        guest.setBooking(null);
        bookingRepository.save(booking);
    }
}
