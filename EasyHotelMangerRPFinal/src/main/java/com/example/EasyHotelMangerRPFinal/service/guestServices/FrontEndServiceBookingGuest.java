package com.example.EasyHotelMangerRPFinal.service.guestServices;

import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.BookingDTO;
import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.CheckAvailableRoomsDTO;
import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.GetAvailableRoomsDTO;
import com.example.EasyHotelMangerRPFinal.exception.ResourceNotFoundException;
import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import com.example.EasyHotelMangerRPFinal.model.booking.BookingPriceCalculator;
import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import com.example.EasyHotelMangerRPFinal.model.hotel.RoomCategory;
import com.example.EasyHotelMangerRPFinal.model.person.Guest;
import com.example.EasyHotelMangerRPFinal.repository.BookingRepository;
import com.example.EasyHotelMangerRPFinal.repository.GuestRepository;
import com.example.EasyHotelMangerRPFinal.repository.RoomRepository;
import com.example.EasyHotelMangerRPFinal.service.officeServices.BookingService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service

public class FrontEndServiceBookingGuest {
    @Autowired
    RoomRepository roomRepository;
    @Autowired
    BookingRepository bookingRepository;
    @Autowired
    BookingService bookingService;
    @Autowired
    GuestRepository guestRepository;

    // get List of available Rooms between planed Check In & Check Out
    public List<GetAvailableRoomsDTO> listOfAvailableRoomsBetween(CheckAvailableRoomsDTO checkAvailableRoomsDTO) {
        ModelMapper modelMapper = new ModelMapper();
        BookingPriceCalculator bookingPriceCalculator = new BookingPriceCalculator();

        LocalDate checkIn = checkAvailableRoomsDTO.getCheckIn();
        LocalDate checkOut = checkAvailableRoomsDTO.getCheckOut();
        RoomCategory roomCategory = checkAvailableRoomsDTO.getRoomCategory();
        int guests = checkAvailableRoomsDTO.getNumberOfGuests();
        //initializing DTO - List of rooms
        List<GetAvailableRoomsDTO> getAvailableRoomsDTOList = new ArrayList<>();
        //filter available rooms between check in & check out
        List<Room> availableRoomsList = roomRepository.findAll()
                .stream()
                .filter(r -> r.getBookingList()
                        .stream()
                        .allMatch(b -> bookingService.isAvailableBetween(checkIn, checkOut, r)))
                .toList();
        //filter room list by room category if chosen
        if (roomCategory != null) {
            availableRoomsList = availableRoomsList.stream()
                    .filter(r -> r.getRoomCategory() == roomCategory).toList();
        }
        //filter room list by number of guests if chosen
        if (guests != 0) {
            availableRoomsList = availableRoomsList.stream()
                    .filter(r -> r.getNumberOfGuests() >= guests).toList();
        }
        //fill DTO-list with rooms
        for (Room r : availableRoomsList) {
            getAvailableRoomsDTOList.add(modelMapper.map(r, GetAvailableRoomsDTO.class));
        }
        //set predicted bookingprice for every room DTO
        getAvailableRoomsDTOList.stream().forEach(r -> r.setTotalPriceForBooking
                (bookingPriceCalculator.calculateBookingPrice(checkIn, checkOut, roomRepository.findById(r.getId()).get())));
        //set booking time span for every room DTO
        getAvailableRoomsDTOList.stream().forEach(r -> r.setBookedDays(ChronoUnit.DAYS.between(checkIn, checkOut)));

        return getAvailableRoomsDTOList;
    }

    //post new booking
    public void postBooking(BookingDTO bookingDTO) {
        ModelMapper modelMapper = new ModelMapper();
        Booking booking = modelMapper.map(bookingDTO, Booking.class);

        LocalDate checkIn = bookingDTO.getCheckIn();
        LocalDate checkOut = bookingDTO.getCheckOut();

        Room room = roomRepository.findById(booking.getRoom().getId()).get();

        if (room.getNumberOfGuests() >= booking.getPlannedNumberOfGuests()) {
            if (bookingService.isAvailableBetween(checkIn, checkOut, room)) {

                BookingPriceCalculator bookingPriceCalculator = new BookingPriceCalculator();
                room.addBooking(booking);
                booking.setBookedDays(ChronoUnit.DAYS.between(checkIn, checkOut));
                booking.setPriceTotal(bookingPriceCalculator.calculateBookingPrice(checkIn, checkOut, room));
                booking.setPricePerNight(bookingPriceCalculator.calculatePricePerNight(checkIn, checkOut, room));
                bookingRepository.save(booking);
            } else {
                System.out.println("Error while postBookingFE(): Room is already booked!");
            }
        } else {
            System.out.println("Error while postBookingFE(): Room cannot contain more guests!");
        }
    }

    //get booking by id
    public ResponseEntity<Booking> getBookingById(long bookingId, long guestId) {
        Booking booking = bookingRepository.findById(bookingId)
                .orElseThrow(() -> new ResourceNotFoundException("Booking with id " + bookingId + " not found"));
        Guest guest = guestRepository.findById(guestId)
                .orElseThrow(() -> new ResourceNotFoundException("Guest with id " + bookingId + " not found"));

        if (bookingRepository.findById(bookingId).get().getGuestList().contains(guestRepository.findById(guestId).get())) {
            return ResponseEntity.ok(booking);
        } else {
            System.out.println("Error occurred at FrontEndServiceBookingGuest -> getBookingById(): " +
                    "the booking you want to see is not yours! Please check your booking details and try again.");
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
    }
}
