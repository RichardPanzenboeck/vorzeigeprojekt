package com.example.EasyHotelMangerRPFinal.service.adminOnlyServices;

import com.example.EasyHotelMangerRPFinal.dto.hotelDTOs.HotelDTO;
import com.example.EasyHotelMangerRPFinal.exception.ResourceNotFoundException;
import com.example.EasyHotelMangerRPFinal.model.hotel.Hotel;
import com.example.EasyHotelMangerRPFinal.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.temporal.ChronoUnit;
import java.util.List;

@Service

public class HotelService {
    @Autowired
    HotelRepository hotelRepository;

    //get all hotels
    public List<Hotel> getAllHotels(){return hotelRepository.findAll();}

    //get hotel by id
    public ResponseEntity<Hotel> getHotelById(@PathVariable long id){
        Hotel hotel = hotelRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("Hotel with id "+id+" doesn´t exist!"));
        return ResponseEntity.ok(hotel);
    }

    //post new hotel
    public void postHotel(Hotel hotel){
        hotelRepository.save(hotel);
    }

    //delete hotel by id
    public void deleteHotel(@PathVariable long id){
        Hotel hotel = hotelRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("Hotel with id "+id+" doesn´t exist!"));

        hotel.getRoomList().stream().forEach(e -> e.setHotel(null));
        hotelRepository.delete(hotel);
    }

    //put hotel DTO
    public void putHotel(@PathVariable long id, HotelDTO hotelDTO){
        Hotel hotel = hotelRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Hotel with id " + id + " doesn´t exist!"));

        if(!hotelRepository.findById(id).get().getName().equals(hotelDTO.getName()))
        hotel.setName(hotelDTO.getName());
        if(hotelRepository.findById(id).get().getMainSeasonStart() != hotelDTO.getMainSeasonStart() ||
                hotelRepository.findById(id).get().getMainSeasonEnd() != hotelDTO.getMainSeasonEnd()) {
            hotel.setMainSeasonStart(hotelDTO.getMainSeasonStart());
            hotel.setMainSeasonEnd(hotelDTO.getMainSeasonEnd());
            hotel.setDaysOfMainSeason(ChronoUnit.DAYS.between(hotelDTO.getMainSeasonStart(), hotelDTO.getMainSeasonEnd()));
        }
        hotelRepository.save(hotel);
    }
}
