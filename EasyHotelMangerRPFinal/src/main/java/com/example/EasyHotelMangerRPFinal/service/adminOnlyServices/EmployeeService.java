package com.example.EasyHotelMangerRPFinal.service.adminOnlyServices;

import com.example.EasyHotelMangerRPFinal.dto.hotelDTOs.EmployeeDTO;
import com.example.EasyHotelMangerRPFinal.exception.ResourceNotFoundException;
import com.example.EasyHotelMangerRPFinal.model.hotel.SalaryCalculator;
import com.example.EasyHotelMangerRPFinal.model.person.Employee;
import com.example.EasyHotelMangerRPFinal.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.util.List;

@Service

public class EmployeeService {
    @Autowired
    EmployeeRepository employeeRepository;

    //get all employees
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    //get employee by id
    public ResponseEntity<Employee> getEmployeeById(@PathVariable long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee with id " + id + " doesn´t exist!"));

        return ResponseEntity.ok(employee);
    }

    //delete employee by id
    public void deleteEmployee(@PathVariable long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee with id " + id + "doesn´t exist!"));
        employeeRepository.delete(employee);
    }

    //post new employee
    public void postEmployee(EmployeeDTO employeeDTO) {
        Employee employee = convertEmployeeDTOtoEmployee(employeeDTO);
        employeeRepository.save(employee);
    }

    //put employee DTO
    public void putEmployee(long id, EmployeeDTO employeeDTO) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee with id " + id + "doesn´t exist!"));

    //Method "convertEmployeeDTOtoEmployee" does not work, changeable attributes had to be implemented separately

        employee.setFirstName(employeeDTO.getFirstName());
        employee.setSalutation(employeeDTO.getSalutation());
        employee.setLastName(employeeDTO.getLastName());
        employee.setMainLanguage(employeeDTO.getMainLanguage());
        employee.setForeignLanguage1(employeeDTO.getForeignLanguage1());
        employee.setForeignLanguage1Level(employeeDTO.getForeignLanguage1Level());
        employee.setForeignLanguage2Level(employeeDTO.getForeignLanguage2Level());
        employee.setForeignLanguage2(employeeDTO.getForeignLanguage2());
        employee.setStartOfEmployment(employeeDTO.getStartOfEmployment());
        employee.setEndOfEmployment(employeeDTO.getEndOfEmployment());
        employee.setYearsOfEmployment(getYearsOfEmployment(employee));
        employee.setSalary(employeeDTO.getSalary());
        employee.setEMailAddress(employeeDTO.getEMailAddress());
        employee.setJob(employeeDTO.getJob());
        employee.setYearsOfExperience(employeeDTO.getYearsOfExperience());
        employee.setHotel(employeeDTO.getHotel());

        SalaryCalculator salaryCalculator = new SalaryCalculator();
        employee.setSalary(salaryCalculator.calculateSalary(employee));

        employeeRepository.save(employee);
    }

    //Method calculates the years of employment by LocalDate.now() and start of employment
    public int getYearsOfEmployment(Employee employee){
        return LocalDate.now().compareTo(employee.getStartOfEmployment());}

    //Method for postEmployee, converting employeeDTO into employee to make it savable for database
    public Employee convertEmployeeDTOtoEmployee(EmployeeDTO employeeDTO){
        Employee employee = new Employee();

        employee.setFirstName(employeeDTO.getFirstName());
        employee.setSalutation(employeeDTO.getSalutation());
        employee.setLastName(employeeDTO.getLastName());
        employee.setMainLanguage(employeeDTO.getMainLanguage());
        employee.setForeignLanguage1(employeeDTO.getForeignLanguage1());
        employee.setForeignLanguage1Level(employeeDTO.getForeignLanguage1Level());
        employee.setForeignLanguage2Level(employeeDTO.getForeignLanguage2Level());
        employee.setForeignLanguage2(employeeDTO.getForeignLanguage2());
        employee.setStartOfEmployment(employeeDTO.getStartOfEmployment());
        employee.setEndOfEmployment(employeeDTO.getEndOfEmployment());
        employee.setYearsOfEmployment(getYearsOfEmployment(employee));
        employee.setSalary(employeeDTO.getSalary());
        employee.setEMailAddress(employeeDTO.getEMailAddress());
        employee.setJob(employeeDTO.getJob());
        employee.setYearsOfExperience(employeeDTO.getYearsOfExperience());
        employee.setHotel(employeeDTO.getHotel());

        SalaryCalculator salaryCalculator = new SalaryCalculator();
        employee.setSalary(salaryCalculator.calculateSalary(employee));

        return employee;
    }
}
