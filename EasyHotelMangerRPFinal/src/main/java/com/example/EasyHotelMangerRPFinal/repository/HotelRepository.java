package com.example.EasyHotelMangerRPFinal.repository;

import com.example.EasyHotelMangerRPFinal.model.hotel.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long> {
}
