package com.example.EasyHotelMangerRPFinal.repository;

import com.example.EasyHotelMangerRPFinal.model.booking.Storno;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StornoRepository extends JpaRepository<Storno, Long> {
}
