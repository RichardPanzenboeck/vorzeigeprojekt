package com.example.EasyHotelMangerRPFinal.repository;

import com.example.EasyHotelMangerRPFinal.model.person.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
