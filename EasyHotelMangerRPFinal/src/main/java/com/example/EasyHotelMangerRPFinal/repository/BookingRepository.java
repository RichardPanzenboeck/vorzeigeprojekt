package com.example.EasyHotelMangerRPFinal.repository;

import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepository extends JpaRepository <Booking, Long> {
}
