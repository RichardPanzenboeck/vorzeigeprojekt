package com.example.EasyHotelMangerRPFinal.repository;

import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
}
