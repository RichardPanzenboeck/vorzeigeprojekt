package com.example.EasyHotelMangerRPFinal.dto.hotelDTOs;

import com.example.EasyHotelMangerRPFinal.model.hotel.Hotel;
import com.example.EasyHotelMangerRPFinal.model.person.Jobs;
import com.example.EasyHotelMangerRPFinal.model.person.Language;
import com.example.EasyHotelMangerRPFinal.model.person.LanguageLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Data
@NoArgsConstructor
@AllArgsConstructor

public class EmployeeDTO {
    private long id;
    private String salutation;
    private String firstName;
    private String lastName;
    private String eMailAddress;
    private String phoneNumber;
    private Jobs job;
    private Language mainLanguage;
    private Language foreignLanguage1;
    private LanguageLevel foreignLanguage1Level;
    private Language foreignLanguage2;
    private LanguageLevel foreignLanguage2Level;
    private LocalDate startOfEmployment;
    private LocalDate endOfEmployment;
    private Hotel hotel;
    private int yearsOfExperience;
    private double salary;
}
