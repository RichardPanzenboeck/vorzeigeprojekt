package com.example.EasyHotelMangerRPFinal.dto.bookingDTOs;

import com.example.EasyHotelMangerRPFinal.model.hotel.RoomCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class GetAvailableRoomsDTO {
    private long id;
    private int numberOfRooms;
    private String numberOfBeds;
    private int numberOfGuests;
    private RoomCategory roomCategory;
    private double offSeasonPricePerNight;
    private double mainSeasonPricePerNight;
    private long bookedDays;
    private double totalPriceForBooking;
}
