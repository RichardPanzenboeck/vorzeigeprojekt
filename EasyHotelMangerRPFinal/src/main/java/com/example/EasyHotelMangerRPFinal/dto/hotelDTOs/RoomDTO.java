package com.example.EasyHotelMangerRPFinal.dto.hotelDTOs;

import com.example.EasyHotelMangerRPFinal.model.hotel.Hotel;
import com.example.EasyHotelMangerRPFinal.model.hotel.RoomCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class RoomDTO {
    private RoomCategory roomCategory;
    private double offSeasonPricePerNight;
    private double mainSeasonPricePerNight;
    private int numberOfGuests;
    private String numberOfBeds;
    private Hotel hotel;
}
