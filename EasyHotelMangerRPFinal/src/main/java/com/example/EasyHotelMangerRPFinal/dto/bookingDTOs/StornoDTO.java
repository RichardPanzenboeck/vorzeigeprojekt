package com.example.EasyHotelMangerRPFinal.dto.bookingDTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class StornoDTO {
    private LocalDate dateOfReservation;
    private LocalDate dateOfStorno;
    private LocalDate dateOfPlannedCheckin;
    private double bookingSum;
    private double stornoSum;
}
