package com.example.EasyHotelMangerRPFinal.dto.hotelDTOs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class HotelDTO {
    private String name;
    private LocalDate mainSeasonStart;
    private LocalDate mainSeasonEnd;
    private long daysOfMainSeason;
}
