package com.example.EasyHotelMangerRPFinal.dto.bookingDTOs;

import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import com.example.EasyHotelMangerRPFinal.model.person.Guest;
import com.example.EasyHotelMangerRPFinal.service.officeServices.BookingService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class BookingDTO {
    private long id;
    private LocalDate dateOfReservation;
    private LocalDate checkIn;
    private LocalDate checkOut;
    private int plannedNumberOfGuests;
    private Room room;
    private Booking booking;
    private List<Guest> guestList = new ArrayList<>();
}

