package com.example.EasyHotelMangerRPFinal.dto.bookingDTOs;

import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import com.example.EasyHotelMangerRPFinal.model.person.Payment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class GuestDTO {
    private String salutation;
    private String firstName;
    private String lastName;
    private String eMailAddress;
    private String phoneNumber;
    private int age;
    private Payment payment;
    private Booking booking;
}
