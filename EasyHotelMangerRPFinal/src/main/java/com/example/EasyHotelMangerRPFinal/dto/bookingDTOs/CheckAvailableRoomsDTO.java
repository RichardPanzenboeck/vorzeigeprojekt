package com.example.EasyHotelMangerRPFinal.dto.bookingDTOs;

import com.example.EasyHotelMangerRPFinal.model.hotel.RoomCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class CheckAvailableRoomsDTO {
    private LocalDate checkIn;
    private LocalDate checkOut;
    private RoomCategory roomCategory;
    private int numberOfGuests;
}
