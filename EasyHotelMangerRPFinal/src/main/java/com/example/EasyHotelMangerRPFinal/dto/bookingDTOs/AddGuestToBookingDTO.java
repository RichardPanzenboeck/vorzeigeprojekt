package com.example.EasyHotelMangerRPFinal.dto.bookingDTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class AddGuestToBookingDTO {
    private long bookingId;
    private long guestId;
}
