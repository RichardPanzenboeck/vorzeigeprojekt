package com.example.EasyHotelMangerRPFinal.dto.hotelDTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class ChangeRoomPricePerNightDTO {
    private long roomId;
    private double roomPricePerNight;
}
