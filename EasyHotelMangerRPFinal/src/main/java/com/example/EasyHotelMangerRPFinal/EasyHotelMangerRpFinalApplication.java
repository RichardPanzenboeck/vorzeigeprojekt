package com.example.EasyHotelMangerRPFinal;

import com.example.EasyHotelMangerRPFinal.controller.adminOnlyController.EmployeeController;
import com.example.EasyHotelMangerRPFinal.controller.adminOnlyController.HotelController;
import com.example.EasyHotelMangerRPFinal.controller.adminOnlyController.RoomController;
import com.example.EasyHotelMangerRPFinal.controller.guestController.FrontEndControllerBookingGuest;
import com.example.EasyHotelMangerRPFinal.controller.officeController.BookingController;
import com.example.EasyHotelMangerRPFinal.controller.officeController.GuestController;
import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.BookingDTO;
import com.example.EasyHotelMangerRPFinal.dto.hotelDTOs.EmployeeDTO;
import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import com.example.EasyHotelMangerRPFinal.model.hotel.Hotel;
import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import com.example.EasyHotelMangerRPFinal.model.hotel.RoomCategory;
import com.example.EasyHotelMangerRPFinal.model.person.*;
import com.example.EasyHotelMangerRPFinal.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;

/**
 * Description:
 * This testversion of EasyHotelManagerRPFinal was made to make Booking processes easier.
 * It allows complete CRUD (Create, Read, Update, Delete)
 * possibilities for hotels, rooms, employees, bookings and stornos for admin and staff,
 * but also booking, guest registration and storno options for customers.
 * <p>
 * In this version are the LocalDate parameters "DateOfStorno" & "DateOfBooking"
 * (in Services and calculators) are set by the tester, to run through different scenarios.
 * This variables have to be set to LocalDate.now(), as soon as the program is rolling out. That change will not affect
 * the rest of the program. If it does, contact me ;D
 *
 * @author Richard Panzenböck
 * @version testversion 1.0, 06.06.2022
 * @apiNote In this test version of the program, this class is just filling the database with dummy-data.
 * some functions only will happen when the program is running, because of the controller laziness.
 * For instance:
 * the List of bookings every room has, just shows something different from zero when you post or put another booking
 * in postman / the front end. Then the bookingList instantly has the correct size, as the numberOfBookings shows.
 */

@SpringBootApplication

public class EasyHotelMangerRpFinalApplication implements CommandLineRunner {

    @Autowired
    RoomRepository roomRepository;
    @Autowired
    HotelRepository hotelRepository;
    @Autowired
    HotelController hotelController;
    @Autowired
    EmployeeController employeeController;
    @Autowired
    GuestController guestController;
    @Autowired
    BookingController bookingController;
    @Autowired
    FrontEndControllerBookingGuest frontEndControllerBookingGuest;

    public static void main(String[] args) {
        SpringApplication.run(EasyHotelMangerRpFinalApplication.class, args);
    }

    public void run(String... args) throws Exception {

        //filling the database with dummy data:

        Hotel hotel = new Hotel("EasyHotel", "Easystreet 22", "1234",
                LocalDate.of(LocalDate.now().getYear(), 6, 1),
                LocalDate.of(LocalDate.now().getYear(), 8, 31));
        hotelController.postHotel(hotel);

        Room room1 = new Room(RoomCategory.CATEGORY1, hotel);
        roomRepository.save(room1);
        hotelRepository.save(hotel);

        Room room2 = new Room(RoomCategory.CATEGORY2, hotel);
        roomRepository.save(room2);
        hotelRepository.save(hotel);

        Room room3 = new Room(RoomCategory.CATEGORY3, hotel);
        roomRepository.save(room3);
        hotelRepository.save(hotel);

        Room room4 = new Room(RoomCategory.CATEGORY4, hotel);
        roomRepository.save(room4);
        hotelRepository.save(hotel);

        EmployeeDTO employee1 = new EmployeeDTO();
        employee1.setHotel(hotel);
        employee1.setFirstName("Heinz");
        employee1.setLastName("Erhardt");
        employee1.setJob(Jobs.CHEF);
        employee1.setMainLanguage(Language.GERMAN);
        employee1.setForeignLanguage1(Language.ENGLISH);
        employee1.setForeignLanguage1Level(LanguageLevel.B2);
        employee1.setForeignLanguage2(Language.FRENCH);
        employee1.setForeignLanguage2Level(LanguageLevel.LOW_LEVEL);
        employee1.setYearsOfExperience(4);
        employee1.setStartOfEmployment(LocalDate.of(2019, 5, 2));
        employeeController.postEmployee(employee1);

        EmployeeDTO employee2 = employee1;
        employee2.setFirstName("Niko");
        employee2.setLastName("Laus");
        employee2.setJob(Jobs.MANAGEMENT);
        employee2.setYearsOfExperience(10);
        employee2.setStartOfEmployment(LocalDate.of(2012, 12, 24));
        employeeController.postEmployee(employee2);

        EmployeeDTO employee3 = employee1;
        employee2.setFirstName("Roland");
        employee2.setLastName("Düringer");
        employee2.setJob(Jobs.RECEPTIONIST);
        employee2.setYearsOfExperience(7);
        employee2.setStartOfEmployment(LocalDate.of(2018, 10, 01));
        employeeController.postEmployee(employee2);

        Guest guest = new Guest();
        guest.setFirstName("Otto");
        guest.setLastName("Konrad");
        guest.setSalutation("Herr");
        guest.setAge(47);
        guest.setPayment(Payment.CREDIT_CARD);
        guest.setEMailAddress("hansi@söllner.com");
        guestController.postGuest(guest);

        Guest guest2 = new Guest();
        guest2.setFirstName("Herman");
        guest2.setLastName("Meier");
        guest2.setSalutation("Herr");
        guest2.setAge(44);
        guest2.setPayment(Payment.PAY_PAL);
        guest2.setEMailAddress("rudi@rüssel.com");
        guestController.postGuest(guest2);

        Guest guest3 = new Guest();
        guest3.setFirstName("Anna");
        guest3.setLastName("Gram");
        guest3.setSalutation("Frau");
        guest3.setAge(33);
        guest3.setPayment(Payment.PAY_PAL);
        guest3.setEMailAddress("anna@gramm.com");
        guestController.postGuest(guest3);

        BookingDTO booking1 = new BookingDTO();
        booking1.setDateOfReservation(LocalDate.of(2021, 1, 5));
        booking1.setCheckIn(LocalDate.of(2021, 2, 2));
        booking1.setCheckOut(LocalDate.of(2021, 2, 10));
        booking1.setRoom(room1);
        booking1.setPlannedNumberOfGuests(2);
        bookingController.postBooking(booking1);

        BookingDTO booking2 = new BookingDTO();
        booking2.setDateOfReservation(LocalDate.of(2021, 1, 5));
        booking2.setCheckIn(LocalDate.of(2021, 3, 2));
        booking2.setCheckOut(LocalDate.of(2021, 3, 10));
        booking2.setRoom(room2);
        booking2.setPlannedNumberOfGuests(3);
        bookingController.postBooking(booking2);

        BookingDTO booking3 = new BookingDTO();
        booking3.setDateOfReservation(LocalDate.of(2021, 5, 5));
        booking3.setCheckIn(LocalDate.of(2021, 5, 27));
        booking3.setCheckOut(LocalDate.of(2021, 6, 4));
        booking3.setRoom(room2);
        booking3.setPlannedNumberOfGuests(3);
        bookingController.postBooking(booking3);

        BookingDTO booking4 = new BookingDTO();
        booking4.setDateOfReservation(LocalDate.of(2021, 5, 5));
        booking4.setCheckIn(LocalDate.of(2021, 6, 10));
        booking4.setCheckOut(LocalDate.of(2021, 6, 18));
        booking4.setRoom(room2);
        booking4.setPlannedNumberOfGuests(3);
        bookingController.postBooking(booking4);

        BookingDTO booking5 = new BookingDTO();
        booking5.setDateOfReservation(LocalDate.of(2021, 5, 5));
        booking5.setCheckIn(LocalDate.of(2021, 8, 28));
        booking5.setCheckOut(LocalDate.of(2021, 9, 5));
        booking5.setRoom(room2);
        booking5.setPlannedNumberOfGuests(3);
        frontEndControllerBookingGuest.postBooking(booking5);
    }
}
