package com.example.EasyHotelMangerRPFinal.controller.officeController;

import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.AddGuestToBookingDTO;
import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.BookingDTO;
import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.CheckAvailableRoomsDTO;
import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import com.example.EasyHotelMangerRPFinal.service.officeServices.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/booking")

public class BookingController {
    @Autowired
    BookingService bookingService;

    //get all bookings
    @CrossOrigin
    @GetMapping
    public List<Booking> getAllBookings() {
        return bookingService.getAllBookings();
    }

    //get available rooms between checkin & checkout
    @CrossOrigin
    @GetMapping("/availableRooms")
    public List<Room> getAvailableRooms(@RequestParam(value = "checkIn") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate checkIn,
                                        @RequestParam(value = "checkOut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate checkOut) {

        CheckAvailableRoomsDTO checkAvailableRoomsDTO = new CheckAvailableRoomsDTO();
        checkAvailableRoomsDTO.setCheckIn(checkIn);
        checkAvailableRoomsDTO.setCheckOut(checkOut);

        return bookingService.listOfAvailableRoomsBetween(checkAvailableRoomsDTO);
    }

    //get booking by id
    @CrossOrigin
    @GetMapping("{id}")
    public ResponseEntity<Booking> getBookingById(@PathVariable long id) {
        return bookingService.getBookingById(id);
    }

    //delete booking by id
    @CrossOrigin
    @DeleteMapping("{id}")
    public void deleteBooking(@PathVariable long id) {
        bookingService.deleteBooking(id);
    }

    //put booking DTO
    @CrossOrigin
    @PutMapping("{id}")
    public void putBooking(@PathVariable long id, @RequestBody BookingDTO bookingDTO) {
        bookingService.putBooking(id, bookingDTO);
    }

    //post new booking
    @CrossOrigin
    @PostMapping
    public void postBooking(@RequestBody BookingDTO bookingDTO) {
        bookingService.postBooking(bookingDTO);
    }

    //add guest to booking
    @CrossOrigin
    @PutMapping("/addGuest")
    public void addGuestToBooking(@RequestParam(value = "bookingId") long bookingId,
                                  @RequestParam(value = "guestId") long guestId){

        AddGuestToBookingDTO addGuestToBookingDTO = new AddGuestToBookingDTO(bookingId, guestId);
        bookingService.addGuestToBooking(addGuestToBookingDTO);
    }

    //remove guest from booking
    @CrossOrigin
    @PutMapping("/removeGuest")
    public void removeGuestFromBooking(@RequestParam(value = "bookingId") long bookingId,
                                       @RequestParam(value = "guestId") long guestId){

        AddGuestToBookingDTO addGuestToBookingDTO = new AddGuestToBookingDTO(bookingId, guestId);
        bookingService.removeGuestFromBooking(addGuestToBookingDTO);
    }
}
