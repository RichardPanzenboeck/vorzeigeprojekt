package com.example.EasyHotelMangerRPFinal.controller.officeController;

import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.StornoDTO;
import com.example.EasyHotelMangerRPFinal.model.booking.Storno;
import com.example.EasyHotelMangerRPFinal.service.officeServices.StornoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/storno")

public class StornoController {
    @Autowired
    StornoService stornoService;

    //get all stornos
    @CrossOrigin
    @GetMapping
    public List<Storno> getAllStornos(){
        return stornoService.getAllStornos();
    }

    //get Storno by Id
    @CrossOrigin
    @GetMapping("{id}")
    public ResponseEntity<Storno> getStornoById(@PathVariable long id){
        return stornoService.getStornoById(id);
    }

    //delete Storno by Id
    @CrossOrigin
    @DeleteMapping("{id}")
    public void deleteStornoById(@PathVariable long id){
        stornoService.deleteStornoById(id);
    }

    //calculate storno
    @CrossOrigin
    @GetMapping("/calculateStorno")
    public StornoDTO getStornoCalculationByBookingId (@RequestParam(value = "id") long id,
                                                      @RequestParam(value ="dateOfStorno")
                                                      @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateOfStorno){

        StornoDTO stornoDTO = new StornoDTO();
        stornoDTO.setDateOfStorno(dateOfStorno);

        return stornoService.getStornoCalculationByBookingId(id, stornoDTO);
    }

    //post new storno
    @CrossOrigin
    @PostMapping
    public void postStorno(@RequestParam(value = "id") long id,
                           @RequestParam(value ="dateOfStorno")
                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateOfStorno){

        StornoDTO stornoDTO = new StornoDTO();
        stornoDTO.setDateOfStorno(dateOfStorno);
        stornoService.postStorno(id, stornoDTO);
    }
}
