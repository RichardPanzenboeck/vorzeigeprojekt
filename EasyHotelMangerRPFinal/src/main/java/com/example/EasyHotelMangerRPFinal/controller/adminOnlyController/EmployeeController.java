package com.example.EasyHotelMangerRPFinal.controller.adminOnlyController;

import com.example.EasyHotelMangerRPFinal.dto.hotelDTOs.EmployeeDTO;
import com.example.EasyHotelMangerRPFinal.model.person.Employee;
import com.example.EasyHotelMangerRPFinal.service.adminOnlyServices.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")

public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    //return all employees
    @CrossOrigin
    @GetMapping
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }

    //return employee by id
    @CrossOrigin
    @GetMapping("{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable long id) {
        return employeeService.getEmployeeById(id);
    }

    //post new employee
    @CrossOrigin
    @PostMapping
    public void postEmployee(@RequestBody EmployeeDTO employeeDTO) {
        employeeService.postEmployee(employeeDTO);
    }

    //delete employee by id
    @CrossOrigin
    @DeleteMapping("{id}")
    public void deleteEmployee(@PathVariable long id) {
        employeeService.deleteEmployee(id);
    }

    //put employee DTO
    @CrossOrigin
    @PutMapping("{id}")
    public void putEmployee(@PathVariable long id, @RequestBody EmployeeDTO employeeDTO) {
        employeeService.putEmployee(id, employeeDTO);
    }
}
