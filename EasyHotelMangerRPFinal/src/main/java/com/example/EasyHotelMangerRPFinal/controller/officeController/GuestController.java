package com.example.EasyHotelMangerRPFinal.controller.officeController;

import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.GuestDTO;
import com.example.EasyHotelMangerRPFinal.model.person.Guest;
import com.example.EasyHotelMangerRPFinal.service.officeServices.GuestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/guest")

public class GuestController {
    @Autowired
    GuestService guestService;

    //return all guests
    @CrossOrigin
    @GetMapping
    public List<Guest> getAllGuests(){
        return guestService.getAllGuests();
    }

    //return guest by id
    @CrossOrigin
    @GetMapping("{id}")
    public ResponseEntity<Guest> getGuestById(@PathVariable long id){
        return guestService.getGuestById(id);
    }

    //post new guest
    @CrossOrigin
    @PostMapping
    public void postGuest(@RequestBody Guest guest){
        guestService.postGuest(guest);
    }

    //delete guest by id
    @CrossOrigin
    @DeleteMapping("{id}")
    public void deleteGuest(@PathVariable long id){
        guestService.deleteGuest(id);
    }

    // put guest DTO
    @CrossOrigin
    @PutMapping("{id}")
    public void putGuest(@PathVariable long id, @Valid @RequestBody GuestDTO guestDTO ){
        guestService.put(id, guestDTO);
    }
}
