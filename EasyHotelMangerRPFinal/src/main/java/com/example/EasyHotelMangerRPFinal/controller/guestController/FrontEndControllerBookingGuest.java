package com.example.EasyHotelMangerRPFinal.controller.guestController;

import com.example.EasyHotelMangerRPFinal.dto.bookingDTOs.*;
import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import com.example.EasyHotelMangerRPFinal.model.hotel.RoomCategory;
import com.example.EasyHotelMangerRPFinal.model.person.Guest;
import com.example.EasyHotelMangerRPFinal.service.officeServices.BookingService;
import com.example.EasyHotelMangerRPFinal.service.guestServices.FrontEndServiceBookingGuest;
import com.example.EasyHotelMangerRPFinal.service.officeServices.GuestService;
import com.example.EasyHotelMangerRPFinal.service.officeServices.StornoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/bookingFrontEnd")

public class FrontEndControllerBookingGuest {
    @Autowired
    FrontEndServiceBookingGuest frontEndServiceBookingGuest;
    @Autowired
    StornoService stornoService;
    @Autowired
    BookingService bookingService;
    @Autowired
    GuestService guestService;

    //get all available Rooms between checkIn & checkOut, independent of planned number of guests.
    //to avoid overbooking a check is implemented in post - booking.
    //additional filtering categories is possible
    @CrossOrigin
    @GetMapping("/availableRooms")
    public List<GetAvailableRoomsDTO> getAvailableRooms(
            @RequestParam(value = "checkIn") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate checkIn,
            @RequestParam(value = "checkOut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate checkOut,
            @Nullable @RequestParam(value="category") RoomCategory roomCategory,
            @Nullable @RequestParam(value="guests") Integer numberOfGuests) {

        CheckAvailableRoomsDTO checkAvailableRoomsDTO = new CheckAvailableRoomsDTO();
        if(checkIn != null)
            checkAvailableRoomsDTO.setCheckIn(checkIn);
        if(checkOut != null)
            checkAvailableRoomsDTO.setCheckOut(checkOut);
        if(roomCategory != null)
            checkAvailableRoomsDTO.setRoomCategory(roomCategory);
        if(numberOfGuests != null)
            checkAvailableRoomsDTO.setNumberOfGuests(numberOfGuests);

        return frontEndServiceBookingGuest.listOfAvailableRoomsBetween(checkAvailableRoomsDTO);
    }

    //create a new booking
    @CrossOrigin
    @PostMapping
    public void postBooking(@RequestBody BookingDTO bookingDTO) {
        frontEndServiceBookingGuest.postBooking(bookingDTO);
    }

    //get booking by id --> alter the bookingdetails (put) is only possible by calling the hotel
    @CrossOrigin
    @GetMapping("/checkBooking")
    public ResponseEntity<Booking> getBookingById(@RequestParam(value = "bookingId") long bookingId, @RequestParam(value = "guestId") long guestId) {
        return frontEndServiceBookingGuest.getBookingById(bookingId, guestId);
    }

    //calculating storno
    @CrossOrigin
    @GetMapping("/calculateStorno")
    public StornoDTO getStornoCalculationByBookingId(@RequestParam(value = "id") long id,
                                                     @RequestParam(value = "dateOfStorno")
                                                     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateOfStorno) {

        StornoDTO stornoDTO = new StornoDTO();
        stornoDTO.setDateOfStorno(dateOfStorno);

        return stornoService.getStornoCalculationByBookingId(id, stornoDTO);
    }

    //post storno
    @CrossOrigin
    @PostMapping("/storno")
    public void postStorno(@RequestParam(value = "id") long id,
                           @RequestParam(value = "dateOfStorno")
                           @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateOfStorno) {

        StornoDTO stornoDTO = new StornoDTO();
        stornoDTO.setDateOfStorno(dateOfStorno);
        stornoService.postStorno(id, stornoDTO);
    }

    //post new guest
    @CrossOrigin
    @PostMapping("createGuest")
    public void postGuest(@RequestBody Guest guest) {
        guestService.postGuest(guest);
    }

    //add guest to booking
    @CrossOrigin
    @PutMapping("/addGuest")
    public void addGuestToBooking(@RequestParam(value = "bookingId") long bookingId,
                                  @RequestParam(value = "guestId") long guestId) {

        AddGuestToBookingDTO addGuestToBookingDTO = new AddGuestToBookingDTO(bookingId, guestId);
        bookingService.addGuestToBooking(addGuestToBookingDTO);
    }

    //remove guest from booking
    @CrossOrigin
    @PutMapping("/removeGuest")
    public void removeGuestFromBooking(@RequestParam(value = "bookingId") long bookingId,
                                       @RequestParam(value = "guestId") long guestId) {

        AddGuestToBookingDTO addGuestToBookingDTO = new AddGuestToBookingDTO(bookingId, guestId);
        bookingService.removeGuestFromBooking(addGuestToBookingDTO);
    }

    // put guest DTO
    @CrossOrigin
    @PutMapping("{id}")
    public void putGuest(@PathVariable long id, @Valid @RequestBody GuestDTO guestDTO ){
        guestService.put(id, guestDTO);
    }
}
