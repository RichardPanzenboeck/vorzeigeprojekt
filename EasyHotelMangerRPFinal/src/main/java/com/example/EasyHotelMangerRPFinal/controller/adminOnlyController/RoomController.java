package com.example.EasyHotelMangerRPFinal.controller.adminOnlyController;

import com.example.EasyHotelMangerRPFinal.dto.hotelDTOs.ChangeRoomPricePerNightDTO;
import com.example.EasyHotelMangerRPFinal.dto.hotelDTOs.RoomDTO;
import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import com.example.EasyHotelMangerRPFinal.service.adminOnlyServices.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/room")

public class RoomController {
    @Autowired
    RoomService roomService;

    //return all rooms
    @CrossOrigin
    @GetMapping
    public List<Room> getAllRooms(){
        return roomService.getAllRooms();
    }

    //return room by id
    @CrossOrigin
    @GetMapping("{id}")
    public ResponseEntity<Room> getRoomById(@PathVariable long id){
        return roomService.getRoomById(id);
    }

    //post new room
    @CrossOrigin
    @PostMapping
    public void postRoom(@RequestParam(value="hotelId") long id, @RequestParam(value="roomCategory") int roomcategory){
        roomService.postRoom(id, roomcategory);
    }

    //put room DTO
    @CrossOrigin
    @PutMapping("{id}")
    public void updateRoom(@PathVariable long id, @RequestBody RoomDTO roomDTO){
        roomService.putRoom(id, roomDTO);
    }

    //change pricePerNight
    @CrossOrigin
    @PutMapping
    public void changeOffSeasonPricePerNight(@RequestParam long roomId, @RequestParam(value ="price") double price){
        ChangeRoomPricePerNightDTO changeRoomPricePerNightDTO = new ChangeRoomPricePerNightDTO(roomId, price);
        roomService.changeOffSeasonPricePerNight(changeRoomPricePerNightDTO);
    }

    //delete room by id
    @CrossOrigin
    @DeleteMapping("{id}")
    public void deleteRoom(@PathVariable long id){
        roomService.deleteRoom(id);
    }
}
