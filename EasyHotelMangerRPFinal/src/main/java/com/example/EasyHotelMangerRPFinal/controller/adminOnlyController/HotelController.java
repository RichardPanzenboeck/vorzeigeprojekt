package com.example.EasyHotelMangerRPFinal.controller.adminOnlyController;

import com.example.EasyHotelMangerRPFinal.dto.hotelDTOs.HotelDTO;
import com.example.EasyHotelMangerRPFinal.model.hotel.Hotel;
import com.example.EasyHotelMangerRPFinal.service.adminOnlyServices.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/hotel")

public class HotelController {
    @Autowired
    private HotelService hotelService;

    //return all hotels
    @CrossOrigin
    @GetMapping
    public List<Hotel> getAllHotels(){
        return hotelService.getAllHotels();
    }

    //return hotel by id
    @CrossOrigin
    @GetMapping("{id}")
    public ResponseEntity<Hotel> getHotelById(@PathVariable long id){
        return hotelService.getHotelById(id);
    }

    //post new hotel
    @CrossOrigin
    @PostMapping
    public void postHotel(@RequestBody Hotel hotel){hotelService.postHotel(hotel);}

    //delete hotel by id
    @CrossOrigin
    @DeleteMapping("{id}")
    public void deleteHotel(@PathVariable long id){
        hotelService.deleteHotel(id);
    }

    //put hotel DTO
    @CrossOrigin
    @PutMapping("{id}")
    public void putHotel(@PathVariable long id, @Valid @RequestBody HotelDTO hotelDTO){
        hotelService.putHotel(id, hotelDTO);
    }
}
