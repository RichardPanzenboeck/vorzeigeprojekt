package com.example.EasyHotelMangerRPFinal.model.hotel;

import com.example.EasyHotelMangerRPFinal.model.person.Employee;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "hotel")

public class Hotel {
    @Id
    @GeneratedValue
    private long id;
    @Column
    private String name;
    @Column
    private String Adress;
    @Column
    private String postalCode;
    @Column
    private int numberOfRooms;
    @Column
    private LocalDate mainSeasonStart;
    @Column
    private LocalDate mainSeasonEnd;
    @Column
    private long daysOfMainSeason;

    @OneToMany(mappedBy = "hotel")
    @JsonManagedReference
    private List<Room> roomList = new ArrayList<>();

    @OneToMany(mappedBy = "hotel")
    @JsonManagedReference
    private List<Employee> employeeList = new ArrayList<>();

    public Hotel(String name, String adress, String postalCode, LocalDate mainSeasonStart, LocalDate mainSeasonEnd) {
        this.name = name;
        this.Adress = adress;
        this.postalCode = postalCode;
        this.numberOfRooms = roomList.size();
        this.mainSeasonStart = mainSeasonStart;
        this.mainSeasonEnd = mainSeasonEnd;
        this.daysOfMainSeason = ChronoUnit.DAYS.between(mainSeasonStart, mainSeasonEnd);
    }

    public void addRoomToList(Room room){
        if(room != null) {
            roomList.add(room);
            setNumberOfRooms(roomList.size());
        }
        else System.out.println("Error occurred in HotelEntity - addtoRoomList(): room is NULL!");
    }
}
