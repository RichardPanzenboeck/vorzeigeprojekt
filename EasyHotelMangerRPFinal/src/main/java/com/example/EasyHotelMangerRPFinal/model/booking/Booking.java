package com.example.EasyHotelMangerRPFinal.model.booking;

import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import com.example.EasyHotelMangerRPFinal.model.person.Guest;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@AllArgsConstructor
@Table(name="booking")

public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private LocalDate dateOfReservation;
    @Column
    private LocalDate checkIn;
    @Column
    private LocalDate checkOut;
    @Column
    private int guestListSize;
    @Column
    private int plannedNumberOfGuests;
    @Column
    private long bookedDays;
    @Column
    private double pricePerNight;
    @Column
    private double priceTotal;
    @Column
    private long bookedRoomId;

    @OneToMany(mappedBy = "booking")
    @JsonManagedReference
    private List<Guest> guestList = new ArrayList<>();

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "room_id")
    private Room room;

    public void addGuest(Guest guest){
        guest.setBooking(this);
        guestList.add(guest);
    }
}
