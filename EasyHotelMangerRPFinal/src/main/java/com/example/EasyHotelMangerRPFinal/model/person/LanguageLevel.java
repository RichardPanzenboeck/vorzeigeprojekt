package com.example.EasyHotelMangerRPFinal.model.person;

public enum LanguageLevel {
    B1,
    B2,
    C1,
    C2,
    LOW_LEVEL
}
