package com.example.EasyHotelMangerRPFinal.model.person;

public enum Payment {
    PAY_PAL,
    CREDIT_CARD,
    ONLINE_BANKING
}
