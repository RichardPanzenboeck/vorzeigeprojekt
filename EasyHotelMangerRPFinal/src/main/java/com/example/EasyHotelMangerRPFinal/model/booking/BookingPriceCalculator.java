package com.example.EasyHotelMangerRPFinal.model.booking;

import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;

import java.time.LocalDate;
import java.time.MonthDay;
import java.time.temporal.ChronoUnit;
import java.util.stream.Stream;

public class BookingPriceCalculator {

    /**
     * This class calculates the total booking price and the booking price per night as well,
     * considering the Local Dates of check in and check out, the room that will be booked and the price increases
     * during the main season.
     *
     * About main season year & check in / check out year:
     * because of the irrelevance of the year from specified main season, the local varibles referenced to main & off season year
     * is changed to the booking year.
     * if the year from the checkin - checkout period differentiates from the main season period, the calculator would not work.
     * in case of a silvester booking, the calculator would not work either.
     *
     * @author  Richard Panzenböck
     * @version 1.0
     *
     * calculating total booking price with 3 parameters Localdates checkin, checkout and room.
     *
     * @param checkIn  is a LocalDate, user given paramater
     * @param checkOut is a LocalDate, user given paramater
     * @param room     is an already existing room of Room.class
     * @return total Price for the booking
     */

    public double calculateBookingPrice(LocalDate checkIn, LocalDate checkOut, Room room) {

        //initialising relevant variables for the calculation:

        double price;
        double mainSeasonPrice = room.getMainSeasonPricePerNight();
        double offSeasonPrice = room.getOffSeasonPricePerNight();

        LocalDate checkin = checkIn;
        LocalDate checkout = checkOut;

        LocalDate mainSeasonStart = room.getHotel().getMainSeasonStart();
        LocalDate mainSeasonEnd = room.getHotel().getMainSeasonEnd();

        if (mainSeasonStart.getYear() != checkin.getYear() || mainSeasonEnd.getYear() != checkout.getYear()) {
            mainSeasonStart = LocalDate.of(checkIn.getYear(), room.getHotel().getMainSeasonStart().getMonth(),
                    room.getHotel().getMainSeasonStart().getDayOfMonth());
            mainSeasonEnd = LocalDate.of(checkIn.getYear(), room.getHotel().getMainSeasonEnd().getMonth(),
                    room.getHotel().getMainSeasonEnd().getDayOfMonth());
        }
        /*
                Mainseason Timeline to check i---------i

                overlapping Timelines of mainseason and period of booking
                checked part marked with |--|
                example:  i--|---|--i
         */
        // i--|---i--|
        if (checkIn.isBefore(mainSeasonEnd) && checkOut.isAfter(mainSeasonEnd)) {
            double offSeasonpriceLocal = ChronoUnit.DAYS.between(checkin, mainSeasonEnd) * offSeasonPrice;
            double mainSeasonpriceLocal = ChronoUnit.DAYS.between(mainSeasonEnd, checkout) * mainSeasonPrice;
            return mainSeasonpriceLocal + offSeasonpriceLocal;
        }
        // |--i---|--i
        else if (checkIn.isBefore(mainSeasonStart) && checkOut.isAfter(mainSeasonStart)) {
            double offSeasonpriceLocal = ChronoUnit.DAYS.between(checkin, mainSeasonStart) * offSeasonPrice;
            double mainSeasonpriceLocal = ChronoUnit.DAYS.between(mainSeasonStart, checkout) * mainSeasonPrice;
            return mainSeasonpriceLocal + offSeasonpriceLocal;
        }
        // i--|---|--i
        else if (checkIn.isAfter(mainSeasonStart) && checkOut.isBefore(mainSeasonEnd)) {
            return ChronoUnit.DAYS.between(checkin, checkout) * mainSeasonPrice;
        }
        // |--i---i--|
        else if (checkIn.isBefore(mainSeasonStart) && checkOut.isAfter(mainSeasonEnd)) {
            double offSeasonpriceLocal = (ChronoUnit.DAYS.between(checkin, mainSeasonStart) +
                    ChronoUnit.DAYS.between(mainSeasonEnd, checkout)) * offSeasonPrice;
            double mainSeasonpriceLocal = ChronoUnit.DAYS.between(mainSeasonStart, mainSeasonEnd) * mainSeasonPrice;
            return mainSeasonpriceLocal + offSeasonpriceLocal;
        }
        // |------|
        else if (checkIn.isEqual(mainSeasonStart) && checkOut.isEqual(mainSeasonEnd)) {
            return ChronoUnit.DAYS.between(mainSeasonStart, checkout) * mainSeasonPrice;
        } else return ChronoUnit.DAYS.between(checkIn, checkOut) * offSeasonPrice;
    }
    /**
     * calculating booking price per night with 3 parameters Localdates checkin, checkout and room.
     *
     * @param checkIn  is a LocalDate, user given paramater
     * @param checkOut is a LocalDate, user given paramater
     * @param room     is an already existing room of Room.class
     * @return Price per night for the booking
     */
    public double calculatePricePerNight(LocalDate checkIn, LocalDate checkOut, Room room) {
        double price = -1;
        double bookingPrice = calculateBookingPrice(checkIn, checkOut, room);
        price = bookingPrice / ChronoUnit.DAYS.between(checkIn, checkOut);

        return price;
    }
}
