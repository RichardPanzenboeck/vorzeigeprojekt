package com.example.EasyHotelMangerRPFinal.model.person;

import com.example.EasyHotelMangerRPFinal.model.hotel.Hotel;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="employee")

public class Employee extends AbstractPerson{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Enumerated(EnumType.STRING)
    private Jobs job;
    @Enumerated(EnumType.STRING)
    private Language mainLanguage;
    @Enumerated(EnumType.STRING)
    private Language foreignLanguage1;
    @Enumerated(EnumType.STRING)
    private LanguageLevel foreignLanguage1Level;
    @Enumerated(EnumType.STRING)
    private Language foreignLanguage2;
    @Enumerated(EnumType.STRING)
    private LanguageLevel foreignLanguage2Level;
    @Column
    private LocalDate startOfEmployment;
    @Column
    private LocalDate endOfEmployment;
    @Column
    private int yearsOfExperience;
    @Column
    private int yearsOfEmployment;
    @Column
    private double salary;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="hotel_id")
    private Hotel hotel;
}
