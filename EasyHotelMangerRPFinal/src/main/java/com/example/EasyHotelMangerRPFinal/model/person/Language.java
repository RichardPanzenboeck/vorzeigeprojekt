package com.example.EasyHotelMangerRPFinal.model.person;

public enum Language {
    GERMAN,
    ENGLISH,
    SPANISH,
    FRENCH;
}
