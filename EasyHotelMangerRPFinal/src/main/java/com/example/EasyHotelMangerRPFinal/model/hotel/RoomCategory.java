package com.example.EasyHotelMangerRPFinal.model.hotel;


public enum RoomCategory {
    CATEGORY1("1 Double",2, 75,1),
    CATEGORY2("1 Double, 1Extra",3, 95,2),
    CATEGORY3("1Double, 2Extra",4, 125,3),
    CATEGORY4("2Double, 2Extra",6, 175,4);

    private double offSeasonPricePerNight;
    private int numberOfGuests;
    private String numberOfBeds;
    private int numberOfRooms;

   private RoomCategory(String numberOfBeds, int numberOfGuests, double offSeasonPricePerNight, int numberOfRooms ){
       this.numberOfBeds = numberOfBeds;
       this.numberOfGuests = numberOfGuests;
       this.offSeasonPricePerNight = offSeasonPricePerNight;
       this.numberOfRooms = numberOfRooms;
   }

    public String getNumberOfBeds(){return numberOfBeds;}
    public int getNumberOfGuests(){return numberOfGuests;}
    public double getOffSeasonPricePerNight(){return offSeasonPricePerNight;}
    public int getNumberOfRooms(){return numberOfRooms;}
}
