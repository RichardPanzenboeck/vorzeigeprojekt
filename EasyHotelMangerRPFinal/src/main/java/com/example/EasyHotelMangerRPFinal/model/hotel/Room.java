package com.example.EasyHotelMangerRPFinal.model.hotel;

import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import com.example.EasyHotelMangerRPFinal.model.booking.Storno;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "room")

public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Enumerated(EnumType.STRING)
    private RoomCategory roomCategory;
    @Column
    private double offSeasonPricePerNight;
    @Column
    private double mainSeasonPricePerNight;
    @Column
    private int numberOfGuests;
    @Column
    private String numberOfBeds;
    @Column
    private int numberOfRooms;
    @Column
    private int numberOfBookings;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "hotel")
    private Hotel hotel;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @JsonManagedReference
    private List<Booking> bookingList = new ArrayList<>();

    @OneToMany(mappedBy = "room")
    @JsonManagedReference
    private List<Storno> stornoList = new ArrayList<>();

    public Room(RoomCategory roomCategory, Hotel hotel) {
        if (roomCategory != null) {
            this.offSeasonPricePerNight = roomCategory.getOffSeasonPricePerNight();
            this.mainSeasonPricePerNight = roomCategory.getOffSeasonPricePerNight() * 1.1;
            this.roomCategory = roomCategory;
            this.numberOfGuests = roomCategory.getNumberOfGuests();
            this.numberOfBeds = roomCategory.getNumberOfBeds();
            this.numberOfRooms = roomCategory.getNumberOfRooms();
        }
        if (hotel != null) {
            this.hotel = hotel;
            hotel.addRoomToList(this);
        }
    }

    public void addBooking(Booking booking){
        bookingList.add(booking);
        booking.setBookedRoomId(this.id);
        setNumberOfBookings(bookingList.size());
    }

    public void setOffSeasonPricePerNightByRoomCateogry(RoomCategory roomCategory) {
        if (roomCategory != null)
            this.offSeasonPricePerNight = roomCategory.getOffSeasonPricePerNight();
        else
            System.out.println("cannot calculate offSeasonPrice per night in roomEntity --> please set roomcategorie first!");
    }

    public void setMainSeasonPricePerNightByRoomCategory(RoomCategory roomCategory) {
        if (roomCategory != null)
            this.mainSeasonPricePerNight = roomCategory.getOffSeasonPricePerNight() * 1.1;
        else
            System.out.println("cannot calculate mainSeasonPrice per night in roomEntity --> please set roomcategorie first!");
    }
}
