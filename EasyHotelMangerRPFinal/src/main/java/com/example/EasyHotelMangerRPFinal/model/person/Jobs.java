package com.example.EasyHotelMangerRPFinal.model.person;

public enum Jobs {
    CHEF(3000),
    SERVICE(2150),
    CLEANING(1560),
    MANAGEMENT(3670),
    RECEPTIONIST(2200);

    private double salary;

    private Jobs(double salary){
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Jobs{" +
                "salary=" + salary +
                '}';
    }

    public double getSalary(){
        return salary;
    }
}
