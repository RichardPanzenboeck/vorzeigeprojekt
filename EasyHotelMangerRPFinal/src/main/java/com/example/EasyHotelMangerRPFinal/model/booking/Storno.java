package com.example.EasyHotelMangerRPFinal.model.booking;

import com.example.EasyHotelMangerRPFinal.model.hotel.Room;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "storno")

public class Storno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private LocalDate dateOfReservation;
    @Column
    private LocalDate dateOfStorno;
    @Column
    private LocalDate dateOfPlannedCheckin;
    @Column
    private double bookingSum;
    @Column
    private double stornoSum;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "room_id")
    private Room room;
}
