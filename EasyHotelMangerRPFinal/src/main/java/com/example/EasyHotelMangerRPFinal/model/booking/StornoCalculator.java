package com.example.EasyHotelMangerRPFinal.model.booking;


import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class StornoCalculator {

    /**
     * *  it is not exactly defined how much the storno is between "storno within 7 days after booking"
     *    and "more than 3 weeks before check in", so I implemented a solution with 100% storno for those possibilities,
     *    50% storno between 2 and 3 weeks and 30% storno between 1 and 2 weeks.
     *
     * @param booking is an already existing booking, needed to calculate prices.
     * @param dateOfStorno is an user given LocalDate in this testversion, to make different test scenarios possible.
     *                     in a later version of this program this value will be fixed to LocalDate.now().
     * @return returns the total storno sum, depending on the difference between storno date and planed check in
     */
    public double calculatingStorno(Booking booking, LocalDate dateOfStorno) {

        //initializing relevant variables
        double storno = -1;
        LocalDate dateOfReservation = booking.getDateOfReservation();
        LocalDate checkin = booking.getCheckIn();
        LocalDate checkout = booking.getCheckOut();
        double bookingPrice = booking.getPriceTotal();

        //if date of Storno is within 7 Days of dateOfReservation storno = 100%
        if (ChronoUnit.DAYS.between(dateOfReservation, dateOfStorno) <= 7 ||
                ChronoUnit.DAYS.between(dateOfStorno, checkin) >= 21) {
            storno = bookingPrice;
        }

        //if date of Storno is 3 weeks before checkin storno = 50%
        else if (ChronoUnit.DAYS.between(dateOfStorno, checkin) < 21 &&
                ChronoUnit.DAYS.between(dateOfStorno, checkin) >= 14) {
            storno = bookingPrice * 0.5;
        }

        //if date of Storno is 2 weeks before checkin storno = 30%
        else if (ChronoUnit.DAYS.between(dateOfStorno, checkin) < 14 &&
                ChronoUnit.DAYS.between(dateOfStorno, checkin) > 7) {
            storno = bookingPrice * 0.3;
        }
        //if date of Storno is 1 weeks before checkin storno = 0%
        else {
            storno = 0;
        }
        return storno;
    }
}
