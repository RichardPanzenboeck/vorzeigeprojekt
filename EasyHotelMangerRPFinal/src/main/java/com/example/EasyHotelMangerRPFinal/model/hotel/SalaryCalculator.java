package com.example.EasyHotelMangerRPFinal.model.hotel;

import com.example.EasyHotelMangerRPFinal.model.person.Employee;

/**
 * This class inherits a calculator for the employees salary.
 * @author Richard Panzenböck
 * @version 1.0
 */

public class SalaryCalculator {

    /**
     * This method calculates the salary by job, job experience and years of employment for the employee.
     *
     * @param employee is an already registered, existing employee.
     * @return the employees salary as a double.
     */

    public double calculateSalary(Employee employee){

        double salary= employee.getJob().getSalary();
        int yearsOfExperience = employee.getYearsOfExperience();
        int yearsOfEmployment = employee.getYearsOfEmployment();

        double employmentBonus = 0.03;
        double experienceBonus = 0.02;
        double calulatedSalary = salary;

        for(int i=0; i<yearsOfEmployment;i++){
            calulatedSalary = calulatedSalary +(salary*employmentBonus);
        }

        for(int i=0; i<yearsOfExperience;i++){
            calulatedSalary = calulatedSalary +(salary*experienceBonus);
        }
        return Math.round(100.0 * calulatedSalary) / 100.0;
    }
}
