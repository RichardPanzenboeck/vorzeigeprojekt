package com.example.EasyHotelMangerRPFinal.model.person;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

public class AbstractPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    private String Salutation;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    @Email
    private String eMailAddress;
    @Column
    private String phoneNumber;
}
