package com.example.EasyHotelMangerRPFinal.model.person;

import com.example.EasyHotelMangerRPFinal.model.booking.Booking;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="guest")

public class Guest extends AbstractPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    private int age;
    @Enumerated(EnumType.STRING)
    private Payment payment;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="booking_id")
    private Booking booking;
}
