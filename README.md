﻿# Willkommen bei meinem Vorzeigeprojekt!

 Worum geht es?

Die Anforderung ging von einem Hotel aus, welches den Buchungsprozess automatisieren
und die Mitarbeiterverwaltung vereinfachen soll.

Hierbei waren folgende Punkte zu beachten, bzw. folgende Muss-Ziele umzusetzen:


•	Kostenberücksichtigung der verschiedenen Zimmertypen

•	Kostenberücksichtigung von Haupt – und Nebensaison (Hauptsaison von 1.6 – 31.8,
	in der Hauptsaison 10% Aufschlag auf Zimmerpreis)

•	Zimmerpreiserhöhungen dürfen auf bestehende Buchungen keine Auswirkung haben.

•	Filtermöglichkeit der verschiedenen Zimmerkategorien bei der Buchung.

•	Sicherstellung, dass nicht mehr Personen einem Zimmer zugewiesen werden können, als es Schlafplätze gibt.

•	Bei der Buchung muss Anzahl der Personen, sowie die Personendaten aller Personen aufgenommen werden.

•	Bei Buchung muss eine Auswahl - für den gewählten Zeitraum verfügbare Zimmer - stattfinden.

•	Am Ende des Buchungsvorganges soll Übersicht über getätigte Buchung angezeigt werden:

	Welche(s) Zimmer wurden gebucht
	Kosten des Zimmers
	Buchungszeitraum
	Anzahl der Mitreisenden
	Finaler Preis

•	Stornomöglichkeit – Rückerstattung in %:

	1 Woche nach der Buchung 100%
	3 Wochen vor Reservierung 50%
	2 Wochen vor Reservierung 30%
	1 Woche vor Reservierung 0%

•	Admin soll die Möglichkeit haben:

	Personal
	Erstellen, löschen, bearbeiten, einsehen

	Zimmer
	Erstellen, löschen, bearbeiten, einsehen


Tech Stack:
Sprache:  Java; 
IDE:      IntelliJ; 
Framework: Spring Boot, Maven; 
Datenbank: H2-Console; 
Frontend:  mit Postman getestet; 

Anmerkung:
Dieses Projekt startete in der 10. Ausbildungswoche bei Codersbay.Vienna und wurde von mir innerhalb von 5 Wochen fertiggestellt.
Response Entities, ModelMapper uvm. wurden daher nicht in allen dafür relevanten Methoden verwendet.
Einige Fehlermeldungen bzgl. Falscheingaben werden nur in der Konsole ausgegeben.
Die im Report befindlichen Diagramme zeigen die erste Planung der Umsetzung des Projekts. Beziehungen zwischen 
verschiedenen Tabellen und andere Kleinigkeiten wurden bei der Umsetzung optimiert - wesshalb die Diagramme nicht 100%ig 
auf das umgesetzte Programm zutreffen.